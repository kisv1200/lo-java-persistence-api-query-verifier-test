package se.miun.dsv.jee.jpa;

import java.util.List;

import se.miun.javaee16.models.Event;

public interface IImportVerifier {

	public abstract List<Event> findAllEventsThatOverlapWithOthers();

	public abstract List<String> findFullNamesOfUsersHostingFutureEvents();

	public abstract Long findNumberOfUsersWithMoreThanOneComment();

	public abstract List<Event> findPastEventsInHamburg();

}